
resource "google_compute_instance" "vm_gitlab" {
  name         = "vm-gitlab-runner"
  machine_type = "e2-medium"
  zone         = "asia-southeast2-a"

  tags = ["http-server", "https-server"]
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2204-lts"
      size  = 30
    }
  }

  metadata = {
    ssh-keys = "nando:${file("~/.ssh/id_rsa.pub")}"
  }
  network_interface {
    network = "default"
    access_config {
    }
  }
}
