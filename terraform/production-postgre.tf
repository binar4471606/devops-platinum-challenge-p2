resource "google_sql_database_instance" "postgres_production" {
  name                = "production-postgres"
  database_version    = "POSTGRES_14"
  region              = "asia-southeast2"
  deletion_protection = false

  settings {
    tier      = "db-f1-micro"
    disk_size = 20
    ip_configuration {
      private_network = "projects/linen-rex-246117/global/networks/default"
    }
  }

}

resource "google_sql_database" "database_production" {
  name     = "people-production"
  instance = google_sql_database_instance.postgres_production.name

}

resource "google_sql_user" "production_user" {
  name     = "people-production"
  instance = google_sql_database_instance.postgres_production.name
  password = "people-production"


}
# resource "null_resource" "import-sql" {
#   depends_on = [google_sql_database.database]
#   provisioner "local-exec" {
#     command = "gcloud sql import sql ${google_sql_database_instance.postgres_staging.name} gs://nando-binar-tf-state/crud_db.sql --database=${google_sql_database.database.name}"
#   }

# }
