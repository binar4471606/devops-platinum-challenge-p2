# Binar Devops Class || Platinum Challenge

Hallo perkenalkan nama saya [Diana Zuniarti](https://www.linkedin.com/in/diana-zuniarti-92227017b/). Saya adalah student [Binar Academy](https://www.binaracademy.com/) khususnya di kelas Devops Wave II yang di mentori oleh [Mochammad Syaifuddin Khasani](https://gitlab.com/roboticpuppies).

Repo ini saya buat untuk project terakhir yaitu Platinum Challenge. Enjoy to read it !!



# What's the challenge?
- Terdapat satu kubernetes cluster di GCP (15%)
- Terdapat Ingress dengan Google Cloud Load Balancer (10%)
- Terdapat domain + SSL untuk Ingress (10%)
- Terdapat GitLab Runner di Kubernetes Cluster (15%)
- Terdapat deployment aplikasi ke Kubernetes Cluster menggunakan Gitlab CI/CD Pipeline (Output: .gitlab-ci.yml) (25%)
- Terdapat link dashboard monitoring dan logging (15%)
- Terdapat repository Ansible untuk memasang GitLab Runner (5%)
- Terdapat repository Terraform untuk membuat VM GitLab Runner dan Google Cloud SQL (5%)

# Topology
![topology example](./documentation/topology.png)



# Create dan Build Image

1. Clone Repo 
2. Buat file dengan nama  `Dockerfile` di masing-masing folder backend dan frontend.
2. Backend :
```
FROM node:18.14-alpine

WORKDIR /app/backend
COPY /backend .

RUN npm install

EXPOSE 8080
CMD ["npm", "run","start"]
```
3. Frontend
```
FROM node:18.14-alpine

WORKDIR /app/frontend
COPY /frontend .

RUN npm install
RUN npm run build

EXPOSE 8081
CMD ["npm","run","start"]
```

4. Build image ke docker registry :
```
docker build -t <images_name>:<tagname> .
```
```
# example :
# docker build -t dianazuniarti/frontend-platinum:v1 .

# docker build -t dianazuniarti/fbackend-platinum:v1 .
```
![image docker](./documentation/image.png)


# Create Kubernetes Cluster
1. Login ke Console GCP masuk ke `Kubernetes Enginee` >> `Cluster` >> `Create Cluster`
2. Pada kali ini saya menggunakan 1 Cluster (3 node dg max 6 node). 
![k8s](./documentation/k8s.png)

# Create and Setup Database (Manually)
1. Create database dengan Cloud SQL (PosgreSQL)
2. Buat database untuk `staging` dan `production`
3. Buat juga user baru untuk akses `staging` dan `production`.

  - Staging :
```
Database Name : people
User          : people
Port          : 5432
password      : <your password>
```
  - Production :
```
Database Name : people2
User          : people2
Port          : 5432
password      : <your password>
```
![userdb](./documentation/user.png)
![db](./documentation/db.png)

4. Import database (optional)

```sql
-- for postgresql
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  name varchar(30),
  email varchar(30),
  gender varchar(6)
);

INSERT INTO users (name, email, gender) VALUES
('John Doe', 'john@gmail.com', 'Male'),
('Mark Lee', 'mlee@gmail.com', 'Male'),
('Sofia', 'sofia@gmail.com', 'Female'),
('Michelle', 'mangela@gmail.com', 'Female');

exit
```

# Create Database dengan Terraform (Automation)
1. Definisikan cloud provider pada file `provider.tf`
```
 provider "google" {
     project = "<your project id>"
     region  = "asia-southeast2"
     zone    = "asia-southeast2-a"
 }
```
2. Siapkan module 
```
terraform init
```
3. Script Terraform file `db-instance.tf`
```
resource "google_sql_database_instance" "postgres_staging" {
    name                = "binar"
    database_version    = "POSTGRES_14"
    region              = "asia-southeast2"
    deletion_protection = false

    settings {
        tier      = "db-f1-micro"
        disk_size = 20
        ip_configuration {
        private_network = "projects/xenon-anthem-374311/global/networks/default"
        }
    }
}

resource "google_sql_database" "database_staging" {
     name     = "people"
     instance = google_sql_database_instance.binar.name

}

resource "google_sql_user" "staging_user" {
     name     = "people"
     instance = google_sql_database_instance.binar.name
     password = "people"
}

resource "google_sql_database" "database_staging" {
     name     = "people"
     instance = google_sql_database_instance.binar.name

}

resource "google_sql_user" "staging_user" {
     name     = "people"
     instance = google_sql_database_instance.binar.name
     password = "people"
}

```
4. Cek config 
```
terraform validate
```

5. Pastikan file terraform sudah benar (membuat resource yg sesuai)
```
terraform plan
```
6. Jalankan script 
```
terraform apply
```
7. Cek apakah database sudah terbuat di dashboard GCP. 

![db-binar](./documentation/db-binar.png)

# Setup Reserved External IP dan Domain 
1. Reserved dua External IP pada GCP agar saat nanti staging maupun production akan mengakses ip yang tetap. 
2. Buat subdomain menggunakan ip yang sudah di reserved serbelumnya. 

# Create Deployment File
Untuk proses deployment, buat 2 directory baru yaitu untuk `staging` dan `production`.

1. Create `namespace.yaml` untuk membuat namespace pada staging
2. Create `deployment.yaml` untuk deploy frontend dan backend.
3. Create `service.yaml` untuk access frontend dan backend.
4. Create `secret.yaml` untuk menyimpan data seperti password, nama user dll. 
5. Create `configmap.yaml` untuk menyimpan data yg tidak terlalu private.
6. Create `ingress.yaml` untuk akses networking nya.
7. Create `managedcertficates.yaml` agar bisa akses dengan https

# Setup Helm Runner
1. Create directory untuk Gitlab runner (`helm-runner`)
2. Buat namespace baru untuk gitlab runner
3. Buat secret masukkan registrasi token yang ada pada gitlab kedalam file ini, jangan lupa untuk di encode.
4. Buat file values (contoh scipt ada di https://artifacthub.io/)
5. Apply namespace terlebih dahulu.
```
kubectl apply -f helm-runner/namespace.yaml
```
6. Selanjutnya apply secret
```
kubectl apply -f helm-runner/secret-gitlab.yaml
```
7. Run Gitlab runner menggunakan helm chart 
```
helm install --namespace <namespace> gitlab-runner -f <CONFIG_VALUES_FILE> gitlab/gitlab-runner
```
8. Pastikan pod untuk runner sudah berjalan 
![runner](./documentation/pod-runner.png)

9. Cek juga status di Gitlab > Setting > CI/CD > Runner
![Gitlab runner](./documentation/gitlab-runner.png)


# Setup CI/CD Process
1. Buat file `.gitlab-ci.yml` untuk proses staging dan production.
2. Lakukan mulai dari build image , hingga deploy frontend dan backend ke step staging dan production (untuk detailnya cek script pada repo)
3. Setup varible yang dibutuhkan. 
![var-gilab](./documentation/var-gitlab.png)

Jika semua sudah, Push repo yang ada di lokal ke Gitlab. Dan proses CI/CD akan berjalan. 
Proses akan dimulai dari build image, selanjutnya akan otomatis deploy di step staging. Jika semua berjalan dengan baik, klik proses pada production secara manually.

![pipeline](./documentation/pipeline.png)

# Check the Result
Akses domain production dan staging, pastikan aplikasi berjalan dengan lancar.
- Staging : https://diana-staging.testrsv.online/
![staging](./documentation/stag.png)
- Production : https://diana-production.testrsv.online/
![production](./documentation/prod.png)

Note : pastikan SSL sudah aktif,aplikasi sudah bisa diakses https, dan CRUD juga berfungsi dengan baik.

# Terraform VM instance :
1. tambahkan file vm-instance.yaml (untuk script bisa di cek di repo)

# Monitoring :
![](./documentation/monitoring.png)
# Credit

All credit goes to [M. Fikri](https://www.youtube.com/watch?v=es9_6RFR7wk&t=3336s) as creator of this app.

App used:

[Frontend](https://github.com/mfikricom/Frontend-React-MySQL)
[Backend](https://github.com/mfikricom/Backend-API-Express-MySQL)
